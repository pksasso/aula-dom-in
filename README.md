<div align="center">
<img src="./logo.png" width="300" >
<h1>Processo Seletivo 20.2</h1>
<h2>Tarefa DOM</h2>
</div>

A tarefa será desenvolver a seguinte página em HTML e JS, que aborde os seguintes requisitos:

- Adicionar mensagens com os inputs "Nome" e "Mensagem" do formulário de envio.
- Excluir todas as mensagens de uma vez
- Dispor as mensagens em cards 
- No card, terá:
     - Nome e mensagem
     - Botão deletar que deleta só aquela mensagem em específico
     - Botão editar que abre um formulário de edição daquela mensagem em específico

MVP é um produto minimo viavel, uma versão simplificada do projeto final, que possui algumas das suas funcionalidades e só devemos começar o próximo MVP quando terminarmos o anterior. Nesse projeto, os MVPs serão divididos da seguinte forma:

  - MVP 1: Adicionar, ler as mensagens, e excluir todos
  - MVP 2: Excluir mensagem especifica
  - MVP 3: Editar a mensagem especifica
  - MVP 4: CSS e estilização. 