let list = document.getElementById('list');
let messages = [];
let nextId = 0;

const getIndex = (id) => messages.findIndex(message => message.id == id);

const listMessages = () => {

  let data = '';

  if(messages.length > 0) {
    messages.map(message => {
      data+= '<li class="list-item">';
      data+= `<div class="list-item-wrapper" id="list-item-${message.id}">`
      data+= `<h2 class="message-name">${message.name}</h2>`;
      data+= `<p class="message-text">${message.text}</p>`;
      data+= `<div class="buttons-wrapper">`;
      data+= `<button class="edit-button green-button button" id="edit-button" onClick="toggleListItem(${message.id})">Editar</button>`;
      data+= `<button class="delete-button red-button button" id="delete-button" onClick="deleteMessage(${message.id})">Apagar</button>`;
      data+= `</div>`;
      data+= `</div>`;
      data+= `<div id="edit-form-${message.id}" class="edit-form" style="display:none">`;
      data+= `<p class="edit-title">Edição de mensagem</p>`;
      data+= `<input id="name-edit-${message.id}" class="edit-name input input-edit" type="text" value="${message.name}" >`;
      data+= `<textarea id="text-edit-${message.id}" class="edit-text input input-edit" type="text">${message.text}</textarea>`;
      data+= `<div class="buttons-wrapper">`;
      data+= `<button type="submit "class="save-edit-button green-button button" onClick="editMessage(${message.id})">Salvar</button>`;
      data+= `<button type="submit "class="cancel-edit-button red-button button" onClick="toggleListItem(${message.id})">Cancelar</button>`;
      data+= `</div>`;
      data+= `</div>`;
      data+= `</li>`;
    })
  }
  return list.innerHTML = data;
}

const addMessage = () => {
  let name = document.getElementById('input-name');
  let text = document.getElementById('input-text');

  if(name && text){
    const newMessage = {
      id: nextId+1,
      name: name.value,
      text: text.value
    };
    nextId++;
    messages.push(newMessage);
    
    document.getElementById('input-name').value = '';
    document.getElementById('input-text').value = '';

    listMessages();
  };
};

const toggleListItem = (id) => {
  toggleEdit(id,'list-item-');
  toggleEdit(id,'edit-form-');
}

const toggleEdit = (id, part) => {
  document.getElementById(`${part}${id}`).style.display =='none' ? 
  document.getElementById(`${part}${id}`).style.display ='flex' :
  document.getElementById(`${part}${id}`).style.display ='none'
}

const editMessage = (id) => {
  const index = getIndex(id);
  const oldMessage = messages[index];
  let nameEdit = document.getElementById(`name-edit-${id}`).value;
  let textEdit = document.getElementById(`text-edit-${id}`).value;
  const newMessage = {
    id: oldMessage.id,
    name: nameEdit,
    text: textEdit
  };
  messages[index] = newMessage;
  listMessages();
};

const deleteMessage = (id) => {
  const index = getIndex(id);
  messages.splice(index,1);
  listMessages();
};

const deleteAllMessages = () => {
  messages = [];
  listMessages();
};

listMessages();